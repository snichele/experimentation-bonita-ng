'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
    function ($scope, Phone) {
        $scope.phones = Phone.query();
        $scope.orderProp = 'age';
    }]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
    function ($scope, $routeParams, Phone) {
        $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function (phone) {
            $scope.mainImageUrl = phone.images[0];
        });

        $scope.setImage = function (imageUrl) {
            $scope.mainImageUrl = imageUrl;
        };
    }]);

phonecatControllers.controller('BonitaCtrl', ['$scope', '$http',
    function ($scope, $http) {
        $scope.url = 'http://localhost:8080/bonita/';
        $scope.user = 'walter.bates';
        $scope.pass = 'bpm';

        $scope.log = function () {
            var fullURL = $scope.url + 'loginservice';
            var body = {'username': $scope.user, 'password': $scope.pass, 'redirect': 'false'};
            var headers = {"Content-type": "application/x-www-form-urlencoded"};
alert(fullURL);
            var req = {
                method: 'POST',
                url: fullURL,
                headers: headers,
                data: body
            };
            $http(req).success(function (datas) {
                console.info(datas)
            }).error(function (e) {
                alert('An error occured. Possible causes :\n\
    - You are not running this application through the tomcat bonita server. Copy this application\n\
into /webapps/ of bonita tomcat and access it through http://localhost:8080/experimentation-bonita-ng/app/#/bonita (default bonita tomcat\n\
port.)')
            });

//                alert('');
//                        def portal_login(url, username, password, disable_cert_validation): < br / >
//                        http = httplib2.Http(disable_ssl_certificate_validation = disable_cert_validation) < br / >
//                        API = "/loginservice" < br / >
//                        URL = url + API < br / >
//                        body = {'username': username, 'password': password, 'redirect': 'false'}
//                < br / >
//                        headers = {"Content-type": "application/x-www-form-urlencoded"}
//                < br / >
//                        response, content = http.request(URL, 'POST', headers = headers, body = urllib.urlencode(body)) < br / >
//                        if response.status != 200: < br / >
//                        raise
//                        Exception("HTTP STATUS: " + str(response.status)) < br / >
//                        return response['set-cookie']
        };
    }]);